{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE QuasiQuotes           #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeFamilies          #-}
module Foundation where

import           Control.Monad
import           Database.Persist.Sql
import           Import.NoFoundation
import           Yesod.Auth.Message
import           Yesod.Core.Types
import qualified Yesod.Core.Unsafe    as Unsafe

data App = App
  { appSettings    :: AppSettings
  , appStatic      :: Static
  , appConPool     :: ConnectionPool
  , appHttpManager :: Manager
  , appLogger      :: Logger
  }

mkYesodData "App" $(parseRoutesFile "config/routes")

type Form x = Html -> MForm (HandlerT App IO) (FormResult x, Widget)

type DB a
  = forall (m :: * -> *). (MonadIO m, Functor m) =>
                            ReaderT SqlBackend m a

instance Yesod App where
  approot =
    ApprootRequest $ \app req ->
      case appRoot $ appSettings app of
        Nothing   -> getApprootText guessApproot app req
        Just root -> root
  makeSessionBackend _ =
    Just <$> defaultClientSessionBackend 600 "config/client_session_key.aes"
  yesodMiddleware = defaultYesodMiddleware
  defaultLayout _widget = do
    _master <- getYesod
    defaultLayout [whamlet|<h1>Jaran|]

instance YesodPersistRunner App where
  getDBRunner = defaultGetDBRunner appConPool

instance YesodPersist App where
  type YesodPersistBackend App = SqlBackend
  runDB action = do
    master <- getYesod
    runSqlPool action $ appConPool master

instance YesodAuth App where
  type AuthId App = UserId
  loginDest _ = HomeR
  logoutDest _ = HomeR
  authPlugins _ = []
  authHttpManager = getHttpManager
  redirectToReferer _ = True
  authenticate creds =
    runDB $ do
      x <- getBy $ UniqueUser $ credsIdent creds
      case x of
        Just (Entity uid _) -> return $ Authenticated uid
        Nothing -> return . UserError . IdentifierNotFound $ credsIdent creds

instance YesodAuthPersist App

instance RenderMessage App FormMessage where
  renderMessage _ _ = defaultFormMessage

instance HasHttpManager App where
  getHttpManager = appHttpManager

unsafeHandler :: App -> Handler a -> IO a
unsafeHandler = Unsafe.fakeHandlerGetLogger appLogger

water :: IO ()
water = putStrLn "Jaran"

--data Person = Person
--  { personName          :: Text
--  , personFavoriteColor :: Maybe Text
--  , personEmail         :: Text
--  , personWebsite       :: Maybe Text
--  } deriving (Show)

--data Car = Car
--  { carYear  :: Int
--  , carModel :: Text
--  , carColor :: Maybe Color
--  } deriving (Show, Generic)

--data Color
--  = Red
--  | Green
--  | Blue
--  | Black
--  deriving (Show, Eq, Enum, Bounded, Generic)

--instance ToJSON Color
--instance ToJSON Car

--data Aff = Aff
--instance Yesod Aff
--instance RenderMessage Aff FormMessage where
--  renderMessage _ _ = defaultFormMessage

--instance YesodJquery Aff

{-
mkYesod "Aff" [parseRoutes|
/               HomeR          GET
/person         PersonR        POST
/another/person AnotherPersonR GET
/input          InputR         GET
/car            CarR           POST
/blog           BlogR          GET POST
/leren          LerenR         GET
/sort           SortR          GET
|]
-}

--people :: [AnotherPerson]
--people =
--  [ AnotherPerson "John" 64
--  , AnotherPerson "Tom" 24
--  , AnotherPerson "Jerry" 28
--  , AnotherPerson "Buddy" 20
--  , AnotherPerson "Brent" 24
--  ]

--getSortR :: Handler Html
--getSortR = defaultLayout
--  [whamlet|
--    <p>
--      <a href="?sort=name">Sort by name
--      |
--      <a href="?sort=age">Sort by age
--      |
--      <a href="?">No sort
--    ^{showPeople}
--  |]

--showPeople :: Widget
--showPeople = do
--  msort <- runInputGet $ iopt textField "sort"
--  let people' =
--        case msort of
--          Just "age" -> sortBy (comparing anotherPersonAge) people
--          Just "name" -> sortBy (comparing anotherPersonName) people
--          _ -> people
--  [whamlet|
--    <dl>
--      $forall person <- people'
--        <dt>#{anotherPersonName person}
--        <dt>#{show $ anotherPersonAge person}
--  |]

--getLerenR :: Handler TypedContent
--getLerenR = selectRep $ do
--  provideRep $ return
--    [shamlet|
--      <p>Hello, my name is #{name} and I'm #{age} years old.
--    |]
--  provideRep $ return $ object
--    [ "name" .= name
--    , "age" .= age
--    ]
--  where
--    name = "Bruce Lee" :: Text
--    age = 86 :: Int

--type Form x = Html -> MForm (HandlerT Aff IO) (FormResult x, Widget)

--newtype UserId =
--  UserId Int
--  deriving (Show)

--data Blog = Blog
--  { blogTitle   :: Text
--  , blogContent :: Textarea
--  , blogId      :: UserId
--  , blogPosted  :: UTCTime
--  } deriving (Show)

--formBlog :: UserId -> Form Blog
--formBlog userId = renderDivs $ Blog
--  <$> areq textField  "Title" Nothing
--  <*> areq textareaField "Contents" Nothing
--  <*> pure userId
--  <*> lift (liftIO getCurrentTime)

--getBlogR :: Handler Html
--getBlogR = do
--  let userId = UserId 6
--  ((res, wid), enctype) <- runFormPost $ formBlog userId
--  defaultLayout
--    [whamlet|
--      <p>Previous result: #{show res}
--      <form method=post action=@{BlogR} enctype=#{enctype}>
--        ^{wid}
--        <input type=submit>
--    |]

--postBlogR :: Handler Html
--postBlogR = getBlogR

--personForm :: Form Person
--personForm =
--  renderDivs $
--  Person <$> areq textField "Name" Nothing <*>
--  aopt textField "Favorite Color" Nothing <*>
--  areq emailField "Email" Nothing <*>
--  aopt urlField "Website" Nothing

--data AnotherPerson = AnotherPerson
--  { anotherPersonName :: Text
--  , anotherPersonAge  :: Int
--  } deriving (Show, Generic)

--instance ToJSON AnotherPerson

--anotherPersonForm :: Html -> MForm Handler (FormResult AnotherPerson, Widget)
--anotherPersonForm extra = do
--  (nameres, nameview) <- mreq textField "this is not used" Nothing
--  (ageres, ageview) <- mreq intField "this is not used" Nothing
--  let personres = AnotherPerson <$> nameres <*> ageres
--      widget = do
--        toWidget
--          [lucius|
--            ##{fvId ageview} {
--              width : 3em;
--            }
--          |]
--        [whamlet|
--          #{extra}
--          <p>
--            Hello, my name is #
--            ^{fvInput nameview}
--            \ and I'm #
--            ^{fvInput ageview}
--            \ years old. #
--            <input type=submit value="introduce myself">
--        |]
--  return (personres, widget)

--passwordConfirmField :: Field Handler Text
--passwordConfirmField =
--  Field
--  { fieldParse =
--      \rawValues _fileValues ->
--        case rawValues of
--          [a, b]
--            | a == b -> return $ Right $ Just a
--            | otherwise -> return $ Left "Passwords don't match."
--          [] -> return $ Right Nothing
--          _ -> return $ Left "You have to return two values."
--  , fieldView =
--      \idAttr nameAttr otherAttrs _eResult _isReq ->
--        [whamlet|
--        <input id=#{idAttr} name=#{nameAttr} *{otherAttrs} type=password>
--        <div>Confirm:
--        <input id=#{idAttr}-confirm name=#{nameAttr} *{otherAttrs} type=password>
--      |]
--  , fieldEnctype = UrlEncoded
--  }

--getAnotherPersonR :: Handler Html
--getAnotherPersonR = do
--  defaultLayout
--    [whamlet|
--      <form action=@{InputR}>
--        <p>
--          My name is
--          <input type=text name=name>
--          and I am
--          <input type=text name=age>
--          years old.
--          <input type=submit value="introduce myself">
--    |]

--getInputR :: Handler TypedContent
--getInputR = do
--  anotherPerson <-
--    runInputGet $
--    AnotherPerson <$> ireq textField "name" <*> ireq intField "age"
--  selectRep $ do
--    provideRep $ return $ [shamlet|<p>#{show anotherPerson}|]
--    provideJson anotherPerson

--carAForm :: Maybe Car -> AForm Handler Car
--carAForm mcar =
--  Car
--  <$> areq carYearField "Year" (carYear <$> mcar)
--  <*> areq textField "Model" (carModel <$> mcar)
--  <*> aopt (selectFieldList colors) "Color" (carColor <$> mcar)
--  where
--    carYearField :: Field (HandlerT Aff IO) Int
--    carYearField = checkM inPast $ checkBool (>= 2000) ("Too old!" :: Text) intField
--    inPast :: (Monad m, MonadIO m) => Int -> m (Either Text Int)
--    inPast year = do
--      thisyear <- liftIO thisYear
--      return $
--        if year <= thisyear
--          then Right year
--          else Left ("How's the future, Marty?" :: Text)
--    colors :: [(Text, Color)]
--    colors = map (pack . show &&& id) [minBound .. maxBound]

--thisYear :: IO Int
--thisYear = do
--  now <- getCurrentTime
--  let today = utctDay now
--      (year, _, _) = toGregorian today
--  return $ fromInteger year

--carForm :: Html -> MForm Handler (FormResult Car, Widget)
--carForm = renderTable $ carAForm $ Just $ Car 2010 "Forte" $ Just Black

--getHomeR :: Handler Html
--getHomeR = do
--  (pwid, penct) <- generateFormPost personForm
--  (cwid, cenct) <- generateFormPost carForm
--  ((_pasres, paswid), pasenct) <- runFormGet $ renderDivs $ areq passwordConfirmField "Password" Nothing
--  defaultLayout
--    [whamlet|
--      <p>generated.
--      <form method=post action=@{PersonR} enctype=#{penct}>
--        ^{pwid}
--        <p>write your own button, tho.
--        <button>submit.
--      <p>generated.
--      <form method=post action=@{CarR} enctype=#{cenct}>
--        ^{cwid}
--        <p>write your own button, tho.
--        <button>submit.
--      <p>Result:
--      <form enctype=#{pasenct}>
--        ^{paswid}
--        <input type=submit value="change password">
--    |]

--postPersonR :: Handler Html
--postPersonR = do
--  ((result, widget), enctype) <- runFormPost personForm
--  case result of
--    FormSuccess person -> defaultLayout [whamlet|<p>#{show person}|]
--    _ ->
--      defaultLayout
--        [whamlet|
--          <p>invalid.
--          <form method=post action=@{PersonR} enctype=#{enctype}>
--            ^{widget}
--            <button>submit.
--        |]

--postCarR :: Handler Html
--postCarR = do
--  ((res, wid), enct) <- runFormPost carForm
--  case res of
--    FormSuccess car -> defaultLayout [whamlet|<p>#{show car}|]
--    _ ->
--      defaultLayout
--        [whamlet|
--          <p>invalid.
--          <form method=post action=@{CarR} enctype=#{enct}>
--            ^{wid}
--            <button>submit.
--        |]

--water :: IO ()
--water = warp 5000 Aff
